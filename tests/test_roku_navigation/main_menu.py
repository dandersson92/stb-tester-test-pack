from stbt import FrameObject, match, ocr, Region
from stbt import press, wait_until


def navigate_to(name, *submenus):
    print "Looking for menu item '%s'" % name
    for _ in range(10):
        if wait_until(lambda: Selection().text == name, timeout_secs=2):
            print "Found menu item '%s'" % name
            break
        press("KEY_DOWN")
    else:
        assert False, "Failed to find '%s' after pressing DOWN 10 times" % name

    if submenus:
        print "Entering menu '%s'" % name
        press("KEY_OK")
        assert wait_until(lambda: Selection().text != name)
        navigate_to(*submenus)


class Selection(FrameObject):
    @property
    def is_visible(self):
        return bool(self._selection)

    @property
    def text(self):
        # Exclude the dark border around the matched region -- ocr has trouble
        # with it.
        region = self._selection.extend(x=10, y=10, right=-10, bottom=-10)

        return ocr(frame=self._frame, region=region)

    @property
    def _selection(self):
        left = match("images/menu-selection-left.png", frame=self._frame)
        right = match("images/menu-selection-right.png", frame=self._frame)
        if left and right:
            return Region(x=left.region.x, y=left.region.y,
                right=right.region.right, bottom=right.region.bottom)
        else:
            return None
