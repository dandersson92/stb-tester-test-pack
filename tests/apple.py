from stbt import FrameObject, match, ocr, Region
from stbt import press, wait_until, match_text
from stbt import is_screen_black, get_frame

import time
import cv2


def navigate_to(name):
    print "Looking for menu item '%s'" % name
    for _ in range(5):
        if wait_until(lambda: Menu().selected == name, timeout_secs=2):
            print "Found menu item '%s'" % name
            break
        press("KEY_RIGHT")
    for _ in range(5):
        if wait_until(lambda: Menu().selected == name, timeout_secs=2):
            print "Found menu item '%s'" % name
            break
        press("KEY_LEFT")
    else:
        assert False, "Failed to find '%s' after pressing RIGHT 5 times followed by LEFT 5 times" % name

class Menu(FrameObject):
    @property
    def is_visible(self):
        return bool(self._selected_text)

    @property
    def selected(self):
        return self._selected_text()
    
    def _selected_text(self):
    	self._frame = cv2.imread("tests/images/apple-selected.png")
        region = Region(0, 950, width=1920, height=50)
        while is_screen_black(frame=self._frame): # Issue with Blackmagic, need to refetch frame
            print "Screen is black - text"
            self._frame = get_frame()
        text = ocr(region=region, frame=self._frame)
        return text



def test_that_experiment():
    print Menu().selected


